# Mullvad iptables

This is a simple tool to auto-generate the Iptables rules for any country configuration of the Mullvad OpenVPN servers. This tool works in combination with the script in the [vpn-gateway](https://gitlab.com/marcandreuf/vpn-gateway) repository. While this tool just prepares the Iptables rules, the other scripts in the ‘vpn-gateway’ repository will automate the process to switch the location of the OpenVPN connection.


## Installation.
This tool will re-generate the ‘iptables.sh’ file used in the vpn-gateway. This tool requires Python3.9. The basic installation is simply copying the files ‘gen_ipt_rules.py’ and ‘templage-iptables.sh’ into the ‘/etc/openvpn’ folder.

The ‘templage-iptables.sh’ is provided separately to allow any custom configuration of the other Iptables rules.


## Run:
```bash
cd /etc/openvpn

#For full help and description of the tool use the command.
python3.9 gen_ipt_rules.py -h

#Basic example usage.
# -cp is for the configuration path to the mullvad OpenVpn config files.
# -cf is for the configuration country file for the generation of the iptables rules.
python3.9 gen_ipt_rules.py -cp {mullvad_configs}/mullvad_config_linux -cf mullvad_{country_code}_all.conf

# A helping tool to list all the config files found by the tool.
python3.9 gen_ipt_rules.py -cp {mullvad_configs}/mullvad_config_linux -l True
```
