import logging
import sys
import argparse
import os
import ipaddress
import more_itertools
import functools


def get_ports_ips(file_content):
    ports = set()
    ips = set()
    remote_lines = list(filter(lambda l: l.startswith("remote "), file_content))
    for line in remote_lines:
        items = line.split(' ')
        ips.add(items[1])
        ports.add(items[2])
    return ports, ips


def get_networks(ips):
    networks = set(list(map(lambda i: get_network_address(i).exploded, ips)))
    return functools.reduce(lambda a, b: f'{a},{b}', networks)


def get_network_address(ip):
    return ipaddress.ip_network(f'{ip}/24', strict=False)


def get_port_ranges(ports):
    int_list = list(map(lambda s: int(s), ports))
    sorted_list = sorted(int_list)
    groups = [list(group) for group in more_itertools.consecutive_groups(sorted_list)]
    ranges = list(map(lambda g: _calc_range(g), groups))
    return functools.reduce(lambda a, b: f'{a},{b}', ranges)


def _calc_range(group):
    mi = min(group)
    ma = max(group)
    if mi < ma:
        return f'{mi}:{ma}'
    return f'{mi}'


def get_config_files(files):
    return list(filter(lambda f: f.endswith('.conf'), files))


def gen_iptables(ranges, local_net, networks, dns_serv, net_dev):
    with open("template-iptables.sh") as ipt_file:
        file_source = ipt_file.read()

        ipt_content = file_source.replace('tcp-ports', ranges) \
            .replace('local_network', local_net) \
            .replace('ips', networks) \
            .replace('dns_server', dns_serv)\
            .replace('eth', net_dev)

        with open("iptables.sh", "w") as itb_script:
            itb_script.write(ipt_content)


def main():
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S')
    console_handler = logging.StreamHandler(stream=sys.stdout)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    logger.info('Start iptables rules generation ...')

    user_home = os.path.expanduser('~')
    default_openvpn_configs = user_home + '/mullvad_openvpn'

    parser = argparse.ArgumentParser(description='This is a tool to generate the rules for iptables for all the '
                                                 'Mullvad openvpn servers.',
                                     epilog='For bugs or improvements contact me at linkedin at marcandreuf')

    parser.add_argument('-cp', '--config-config_path', type=str, default=default_openvpn_configs, dest='openvpn_config',
                        help=f'Path to the openvpn Mullvad config files. Default to "user-home/mullvad_openvpn".')

    parser.add_argument('-cf', '--config-file', type=str, default="mullvad_se_all.conf", dest='config_file',
                        help='Config file to setup the VPN connection. Default to "mullvad_se_all.conf"')

    parser.add_argument('-n', '--local-network', type=str, default="192.168.0.0", dest='local_network',
                        help='Local network ip to allow traffic in iptables rules. Default to "192.168.0.0"')

    parser.add_argument('-d', '--dns_server', type=str, default="1.1.1.1", dest='dns_server',
                        help='Dns server ip configured in iptables rules. Default to "1.1.1.1"')

    parser.add_argument('-nd', '--network-device', type=str, default="ens1", dest='network_device',
                        help='Name of the network device. Default to "ens1"')

    parser.add_argument('-l', '--list', type=str, default=False, dest='list',
                        help='Print the list of config files.')

    args = parser.parse_args()
    config_path = args.openvpn_config
    config_file = args.config_file
    local_network = args.local_network
    dns_server = args.dns_server
    net_device = args.network_device

    logger.info(f"Arguments, config files: {config_path}\n"
                f" config file: {config_file}\n"
                f" local network: {local_network}\n"
                f" dns server: {dns_server}\n"
                f" network device: {net_device}.")

    if args.list:
        files = os.listdir(config_path)
        lst_config_files = get_config_files(files)
        for f in lst_config_files:
            print(f)
    else:
        with open(config_path + "/" + config_file) as cf:
            config_content = cf.readlines()
            ports, ips = get_ports_ips(config_content)
            ranges = get_port_ranges(ports)
            networks = get_networks(ips)

            gen_iptables(ranges, local_network, networks, dns_server, net_device)


if __name__ == "__main__":
    main()
