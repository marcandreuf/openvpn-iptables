import logging
import pytest
import os

import gen_ipt_rules

logger = logging.getLogger(__name__)


@pytest.fixture
def sample_conf():
    with open("resources/sample.conf") as file:
        return file.readlines()


def test_fetch_ports_and_ips(sample_conf):
    ports, ips = gen_ipt_rules.get_ports_ips(sample_conf)

    ranges = gen_ipt_rules.get_port_ranges(ports)
    assert ranges == "55,123:125,1800:1801"
    logger.info(ranges)
    networks = gen_ipt_rules.get_networks(ips)
    assert "10.15.21.0/24" in networks
    assert "192.168.0.0/24" in networks
    assert "127.0.0.0/24" in networks
    logger.info(networks)


def test_extract_network_from_ip():
    sample_ip = "123.124.15.2"
    subnet = gen_ipt_rules.get_network_address(sample_ip)
    assert subnet.exploded == "123.124.15.0/24"


def test_get_network_ips():
    sample_ips = {'134.108.67.12', '15.14.98.35', '10.0.82.60', '192.168.0.1', '127.0.0.1'}
    str_networks = gen_ipt_rules.get_networks(sample_ips)
    assert "10.0.82.0/24" in str_networks
    assert "15.14.98.0/24" in str_networks
    assert "192.168.0.0/24" in str_networks


def test_get_ports_ranges():
    sample_ports_set = {'1294', '1801', '1296', '1297', '1802', '1295', '1800', '123', '126', '125', '124', '55'}
    ranges = gen_ipt_rules.get_port_ranges(sample_ports_set)
    assert "1294:1297" in ranges
    assert "1800:1802" in ranges
    assert ", 1800:1802" not in ranges
    assert "55" in ranges
    assert "55:" not in ranges


def test_generate_new_iptables_rules():
    ports = "123:125"
    local_net = "127.0.0.1"
    ips = "127.0.0.1/24"
    dns = "1.1.1.1"
    net_dev = "eth1"
    gen_ipt_rules.gen_iptables(ports, local_net, ips, dns, net_dev)

    with open("iptables.sh") as itb_script:
        content = itb_script.read()
        assert "tcp-ports" not in content
        assert ports in content
        assert "local_network" not in content
        assert local_net in content
        assert "ips" not in content
        assert ips in content
        assert "dns_server" not in content
        assert dns in content
        assert "eth0" not in content
        assert net_dev in content

    os.remove("iptables.sh")
